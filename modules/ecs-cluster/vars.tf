# ---------------------------------------------------------------------------------------------------------------------
# MODULE PARAMETERS
# These variables are expected to be passed in by the operator when calling this terraform module.
# ---------------------------------------------------------------------------------------------------------------------

# General ECS Cluster properties
variable "cluster_name" {
  description = "The name of the ECS cluster (e.g. ecs-prod). This is used to namespace all the resources created by these templates."
}

variable "cluster_min_size" {
  description = "The minimum number of EC2 Instances launchable for this ECS Cluster. Useful for auto-scaling limits."
}

variable "cluster_max_size" {
  description = "The maximum number of EC2 Instances that must be running for this ECS Cluster. We recommend making this twice var.cluster_min_size, even if you don't plan on scaling the cluster up and down, as the extra capacity will be used to deploy udpates to the cluster."
}

# Properties of the ECS Cluster's EC2 Instances
variable "cluster_instance_ami_search" {
#TODO
  description = "The AMI name pattern for each of the ECS Cluster's EC2 Instances."
  default = {
    name = "amzn2-ami-ecs-hvm-*-x86_64-ebs"
    virtualization = "hvm"
    architecture = "x86_64"
    root_device = "ebs"
    owner = "amazon"
  }
}

variable "cluster_instance_ami" {
  description = "The AMI to run on each of the ECS Cluster's EC2 Instances."
  default = "ami-0f846c06eb372f19a"
}

variable "cluster_instance_type" {
  description = "The type of EC2 instance to run for each of the ECS Cluster's EC2 Instances (e.g. t2.medium)."
}

variable "cluster_instance_root_volume_size" {
  description = "The size in GB of the root volume for each of the ECH Cluster's EC2 Instances"
  default     = 8
}

variable "cluster_instance_root_volume_type" {
  description = "The volume type for the root volume for each of the ECH Cluster's EC2 Instances. Can be standard, gp, or io1"
  default     = "standard"
}

variable "cluster_instance_keypair_name" {
  description = "The EC2 Keypair name used to SSH into the ECS Cluster's EC2 Instances."
}

variable "cluster_instance_user_data" {
  description = "The User Data script to run on each of the ECS Cluster's EC2 Instances on their first boot."
}

variable "cluster_instance_spot_price" {
  description = "If set to a non-empty string EC2 Spot Instances will be requested for the ECS Cluster. The value is the maximum bid price for the instance on the EC2 Spot Market."
  default     = ""
}

# Info about the VPC in which this Cluster resides

variable "vpc_id" {
  description = "The ID of the VPC in which the ECS Cluster's EC2 Instances will reside."
}

variable "vpc_subnet_ids" {
  description = "A list of the subnets into which the ECS Cluster's EC2 Instances will be launched. These should usually be all private subnets and include one in each AWS Availability Zone."
  type        = "list"
}

variable "ssh_security_groups" {
  description = "The security group id from which SSH access should be permitted to the ECS Cluster instances. Should typically be the security group id of a bastion host."
  type        = "list"
  default     = []
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL MODULE PARAMETERS
# These variables have defaults, but may be overridden by the operator.
# ---------------------------------------------------------------------------------------------------------------------

variable "ingress_security_groups" {
  description = "Security Group IDs (eg. *LB, Instance) which will send traffic"
  type        = "list"
  default     = []
}

variable "ingress_cidr" {
  description = "CIDR (eg. NLB, VPC peers) which will send traffic"
  type        = "list"
  default     = []
}

variable "attach_security_groups" {
  description = "Security Group IDs to attach to ECS instance"
  type        = "list"
  default     = []
}


variable "tenancy" {
  description = "Tenancy of instances: default (dedicated, host)"
  default     = "default"
}

variable "custom_tags_ec2_instances" {
  description = "A list of custom tags to apply to the EC2 Instances in this ASG. Each item in this list should be a map with the parameters key, value, and propagate_at_launch."
  type        = "list"
  default     = []

  # Example:
  # default = [
  #   {
  #     key = "foo"
  #     value = "bar"
  #     propagate_at_launch = true
  #   },
  #   {
  #     key = "baz"
  #     value = "blah"
  #     propagate_at_launch = true
  #   }
  # ]
}

variable "custom_tags_security_group" {
  description = "A map of custom tags to apply to the Security Group for this ECS Cluster. The key is the tag name and the value is the tag value."
  type        = "map"
  default     = {}

  # Example:
  #   {
  #     key1 = "value1"
  #     key2 = "value2"
  #   }
}

variable "termination_policies" {
  description = "A list of policies to decide how the instances in the auto scale group should be terminated. The allowed values are OldestInstance, NewestInstance, OldestLaunchConfiguration, ClosestToNextInstanceHour, OldestLaunchTemplate, AllocationStrategy, Default. If you specify more than one policy, the ASG will try each one in turn, use it to select the instance(s) to terminate, and if more than one instance matches the criteria, then use the next policy to try to break the tie. E.g., If you use ['OldestInstance', 'ClosestToNextInstanceHour'] and and there were two instances with exactly the same launch time, then the ASG would try the next policy, which is to terminate the one closest to the next instance hour in billing."
  type        = "list"

  # Our default policy is optimized for rolling out updates to the ECS cluster via roll-out-ecs-cluster-update.py.
  # That script scales the cluster up to launch new instances and then back down with the intention of terminating
  # the older instances, so we need to use the OldestInstance policy for that to work.
  default = ["OldestInstance"]
}
