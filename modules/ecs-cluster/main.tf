# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CREATE AN EC2 CONTAINER SERVICE (ECS) CLUSTER
# These templates launch an ECS cluster you can use for running Docker containers. The cluster includes:
# - Auto Scaling Group (ASG)
# - Launch configuration
# - Security group
# - IAM roles and policies
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ---------------------------------------------------------------------------------------------------------------------
# SET TERRAFORM REQUIREMENTS FOR RUNNING THIS MODULE
# ---------------------------------------------------------------------------------------------------------------------

terraform {
  required_version = "~> 0.9"
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE THE ECS CLUSTER ENTITY
# Amazon's ECS Service requires that we create an entity called a "cluster". We will then register EC2 Instances with
# that cluster.
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_ecs_cluster" "ecs" {
  name = "${var.cluster_name}"
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE THE ECS CLUSTER AUTO SCALING GROUP (ASG)
# The ECS Cluster's EC2 Instances (known in AWS as "Container Instances") exist in an Auto Scaling Group so that failed
# instances will automatically be replaced, and we can easily scale the cluster's resources.
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_autoscaling_group" "ecs" {
  depends_on = ["aws_ecs_cluster.ecs"]

  name                 = "${var.cluster_name}"
  min_size             = "${var.cluster_min_size}"
  max_size             = "${var.cluster_max_size}"
  launch_configuration = "${aws_launch_configuration.ecs.name}"
#TODO rename to 'subnet_ids'
  vpc_zone_identifier  = ["${var.vpc_subnet_ids}"]
  termination_policies = ["${var.termination_policies}"]

  tags = ["${concat(module.default_tags.list_value, var.custom_tags_ec2_instances)}"]
}

module "default_tags" {
  source = "git::git@github.com:gruntwork-io/package-terraform-utilities.git//modules/intermediate-variable?ref=v0.0.1"

  list_value = [
    {
      key                 = "Name"
      value               = "${var.cluster_name}"
      propagate_at_launch = true
    },
  ]
}


data "aws_ami" "ecs" {
#TODO use datastructure
  most_recent = true

  filter {
    name   = "name"
    values = [ "amzn2-ami-ecs-*-ebs" ]
  }

  filter {
    name   = "virtualization-type"
    values = [ "hvm" ]
  }

  # Amazon = [ 591542846629, ... ]
  owners = [ "self", "amazon" ] 
}


# Launch Configuration for the ECS Cluster's Auto Scaling Group.
resource "aws_launch_configuration" "ecs" {
  depends_on = ["aws_ecs_cluster.ecs"]

  name_prefix          = "${var.cluster_name}-"

#NOTE
# setting 'most_recent=true' in aws_ami.ecs returns a singleton. Otherwise use element(..., 0) 
  image_id             = "${coalesce(var.cluster_instance_ami, data.aws_ami.ecs.id)}"

  instance_type        = "${var.cluster_instance_type}"
  key_name             = "${var.cluster_instance_keypair_name}"
  security_groups      = [ "${aws_security_group.ecs.id}",
        "${compact(concat(var.attach_security_groups))}" ]

  user_data            = "${var.cluster_instance_user_data}"
  iam_instance_profile = "${aws_iam_instance_profile.ecs.name}"
  placement_tenancy    = "${var.cluster_instance_spot_price == "" ? var.tenancy : ""}"
  spot_price           = "${var.cluster_instance_spot_price}"

  root_block_device {
    volume_size = "${var.cluster_instance_root_volume_size}"
    volume_type = "${var.cluster_instance_root_volume_type}"
  }

  # Important note: whenever using a launch configuration with an auto scaling group, you must set
  # create_before_destroy = true. However, as soon as you set create_before_destroy = true in one resource, you must
  # also set it in every resource that it depends on, or you'll get an error about cyclic dependencies (especially when
  # removing resources). For more info, see:
  #
  # https://www.terraform.io/docs/providers/aws/r/launch_configuration.html
  # https://terraform.io/docs/configuration/resources.html
  lifecycle {
    create_before_destroy = true
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE THE ECS CLUSTER INSTANCE SECURITY GROUP
# Limits which ports are allowed inbound and outbound. We export the security group id as an output so users of this
# module can add their own custom rules.
# ---------------------------------------------------------------------------------------------------------------------

# Note that we do not define ingress and egress rules inline. This is because consumers of this terraform module might
# want to add arbitrary rules to this security group. See:
# https://www.terraform.io/docs/providers/aws/r/security_group.html.
resource "aws_security_group" "ecs" {
  name        = "${var.cluster_name}"
  description = "For EC2 Instances in the ${var.cluster_name} ECS Cluster."
  vpc_id      = "${var.vpc_id}"
  tags        = "${var.custom_tags_security_group}"

  # For an explanation of why this is here, see the aws_launch_configuration.ecs
  lifecycle {
    create_before_destroy = true
  }
}

#TODO rename to _vpc and CIDR is VPC range or at least 'self'
# also define endpoints for all needed services (SSM, EFS, S3)
#
# Allow all outbound traffic from the ECS Cluster
resource "aws_security_group_rule" "allow_outbound_all" {
  type        = "egress"
  from_port   = 0
  to_port     = 65535
  protocol    = "tcp"
  cidr_blocks = [ "0.0.0.0/0" ]
  security_group_id = "${aws_security_group.ecs.id}"
}

#TODO use map
# Allow inbound SSH traffic from the Security Group ID specified in var.allow_ssh_from_security_group_id.
resource "aws_security_group_rule" "allow_inbound_ssh_from_security_group" {
  count = "${length(var.ssh_security_groups)}"

  type                     = "ingress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  source_security_group_id = "${element(var.ssh_security_groups, count.index)}"
  security_group_id        = "${aws_security_group.ecs.id}"
}

resource "aws_security_group_rule" "ingress_self_tcp" {
  type      = "ingress"
  from_port = 32768
  to_port   = 65535
  protocol  = "tcp"
  self      = true
  security_group_id = "${aws_security_group.ecs.id}"
}

resource "aws_security_group_rule" "ingress_self_udp" {
  type      = "ingress"
  from_port = 32768
  to_port   = 65535
  protocol  = "udp"
  self      = true
  security_group_id = "${aws_security_group.ecs.id}"
}

# Assume Docker container expose ports within the "ephemeral" port range. (https://goo.gl/uLs9NY)
# Allow inbound access from SG-tagged (eg. ELB/ALB, instances)
#
resource "aws_security_group_rule" "ingress_sg_tcp" {
  count = "${length(var.ingress_security_groups)}"

  type                     = "ingress"
  from_port                = 32768
  to_port                  = 65535
  protocol                 = "tcp"
  source_security_group_id = "${element(var.ingress_security_groups, count.index)}"
  security_group_id        = "${aws_security_group.ecs.id}"
}

resource "aws_security_group_rule" "ingress_sg_udp" {
  count = "${length(var.ingress_security_groups)}"

  type                     = "ingress"
  from_port                = 32768
  to_port                  = 65535
  protocol                 = "udp"
  source_security_group_id = "${element(var.ingress_security_groups, count.index)}"
  security_group_id        = "${aws_security_group.ecs.id}"
}


# Allow inbound access from CIDR (eg. NLB, VPC Peer)
#
resource "aws_security_group_rule" "ingress_cidr_tcp" {
  count = "${length(var.ingress_cidr) > 0 ? 1 : 0}"

  type          = "ingress"
  from_port     = 32768
  to_port       = 65535
  protocol      = "tcp"
  cidr_blocks   = [ "${var.ingress_cidr}" ]
  security_group_id = "${aws_security_group.ecs.id}"
  description   = "NLB and VPC-local"
}

resource "aws_security_group_rule" "ingress_cidr_udp" {
  count = "${length(var.ingress_cidr) > 0 ? 1 : 0}"

  type          = "ingress"
  from_port     = 32768
  to_port       = 65535
  protocol      = "udp"
  cidr_blocks   = [ "${var.ingress_cidr}" ]
  security_group_id = "${aws_security_group.ecs.id}"
  description   = "NLB and VPC-local"
}


# ---------------------------------------------------------------------------------------------------------------------
# CREATE AN IAM ROLE AND POLICIES FOR THE CLUSTER INSTANCES
# IAM Roles allow us to grant the cluster instances access to AWS Resources. We export the IAM role id so users of this
# module can add their own custom IAM policies.
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_iam_role" "ecs" {
  name               = "${var.cluster_name}-instance"
  assume_role_policy = "${data.aws_iam_policy_document.ecs_role.json}"

  # For an explanation of why this is here, see the aws_launch_configuration.ecs
  lifecycle {
    create_before_destroy = true
  }

  # IAM objects take time to propagate. This leads to subtle eventual consistency bugs where the ECS cluster cannot be
  # created because the IAM role does not exist. We add a 15 second wait here to give the IAM role a chance to propagate
  # within AWS.
  provisioner "local-exec" {
    command = "echo 'Sleeping for 15 seconds to wait for IAM role to be created'; sleep 15"
  }
}

data "aws_iam_policy_document" "ecs_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

# To assign an IAM Role to an EC2 instance, we need to create the intermediate concept of an "IAM Instance Profile".
resource "aws_iam_instance_profile" "ecs" {
  role = "${aws_iam_role.ecs.name}"

  # For an explanation of why this is here, see the aws_launch_configuration.ecs
  lifecycle {
    create_before_destroy = true
  }
}

# IAM policy we add to our EC2 Instance Role that allows an ECS Agent running on the EC2 Instance to communicate with
# an ECS cluster.
resource "aws_iam_role_policy" "ecs" {
  name   = "${var.cluster_name}-ecs-permissions"
  role   = "${aws_iam_role.ecs.id}"
  policy = "${data.aws_iam_policy_document.ecs_permissions.json}"
}

data "aws_iam_policy_document" "ecs_permissions" {
  statement {
    effect = "Allow"

    actions = [
      "ecs:CreateCluster",
      "ecs:DeregisterContainerInstance",
      "ecs:DiscoverPollEndpoint",
      "ecs:Poll",
      "ecs:RegisterContainerInstance",
      "ecs:StartTelemetrySession",
      "ecs:Submit*",
    ]

    resources = ["*"]
  }
}

# IAM policy we add to our EC2 Instance Role that allows ECS Instances to pull all containers from Amazon EC2 Container
# Registry.
resource "aws_iam_role_policy" "ecr" {
  name   = "${var.cluster_name}-docker-login-for-ecr"
  role   = "${aws_iam_role.ecs.id}"
  policy = "${data.aws_iam_policy_document.ecr_permissions.json}"
}

data "aws_iam_policy_document" "ecr_permissions" {
  statement {
    effect = "Allow"

    actions = [
      "ecr:BatchCheckLayerAvailability",
      "ecr:BatchGetImage",
      "ecr:DescribeRepositories",
      "ecr:GetAuthorizationToken",
      "ecr:GetDownloadUrlForLayer",
      "ecr:GetRepositoryPolicy",
      "ecr:ListImages",
    ]

    resources = ["*"]
  }
}
